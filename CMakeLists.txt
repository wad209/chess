cmake_minimum_required(VERSION 3.19)
project(Chess LANGUAGES C CXX ASM)

include(cmake/lto.cmake)
include(cmake/arduino_core.cmake)
include(cmake/arduino_hex.cmake)
include(cmake/arduino_upload.cmake)
include(cmake/arduboy2.cmake)
include(cmake/eeprom.cmake)

# TODO: this shouldn't be necessary, library should automatically include.
INCLUDE_DIRECTORIES(~/Arduino/libraries/Arduboy2/src/)
INCLUDE_DIRECTORIES(~/.arduino15/packages/arduino/hardware/avr/1.8.6/libraries/EEPROM/src/)

add_executable(chess chess.cpp)
target_link_libraries(chess PUBLIC ArduinoCore Arduboy2)
target_compile_options(chess PRIVATE
    "-Wall"
    "-Wextra"
    "-pedantic"
    "-fpermissive"
)
arduino_avr_hex(chess)

set(ARDUINO_PORT "/dev/ttyACM0"
    CACHE STRING "The serial port for uploading using avrdude")
arduino_avr_upload(chess ${ARDUINO_PORT})
