#include <Arduboy2.h>
 
Arduboy2 arduboy;

#define BOARD_BLACK true
#define BOARD_WHITE false

#define AR_EVEN true
#define AR_ODD false

bool get_arity(int num) {
  Serial.print(num);
  Serial.print(" is ");
  if (num % 2 == 0) {
    Serial.print("even");
    Serial.println();
    return AR_EVEN;
  } else {
    Serial.print("odd");
    Serial.println();
    return AR_ODD;
  }
}

const unsigned char PROGMEM WHITE_TILE[] = {
  8, 8,
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

const unsigned char PROGMEM pawn_white_on_black[] = {
  8, 8,
  0x80, 0xc8, 0xec, 0xfe, 0xfe, 0xec, 0xc8, 0x80
};

const unsigned char PROGMEM pawn_white_on_white[] = {
  8, 8,
  0x7f, 0xb7, 0xdb, 0xed, 0xed, 0xdb, 0xb7, 0x7f
};

const unsigned char PROGMEM pawn_black_on_white[] = {
  8, 8,
  0x7f, 0x37, 0x13, 0x1, 0x1, 0x13, 0x37, 0x7f
};

const unsigned char PROGMEM pawn_black_on_black[] = {
  8, 8,
  0x80, 0x48, 0x24, 0x12, 0x12, 0x24, 0x48, 0x80
};

const unsigned char PROGMEM castle_white_on_black[] = {
  8, 8,
  0x0, 0xcc, 0xf8, 0xfc, 0xfc, 0xf8, 0xcc, 0x0
};

const unsigned char PROGMEM castle_black_on_white[] = {
  8, 8,
  0xff, 0x33, 0x7, 0x3, 0x3, 0x7, 0x33, 0xff
};

const unsigned char PROGMEM castle_white_on_white[] = {
  8, 8,
  0xff, 0x33, 0xcf, 0xfb, 0xfb, 0xcf, 0x33, 0xff
};

const unsigned char PROGMEM castle_black_on_black[] = {
  8, 8,
  0x0, 0xcc, 0x30, 0x4, 0x4, 0x30, 0xcc, 0x0
};

const unsigned char PROGMEM knight_white_on_black[] = {
  8, 8,
  0x0, 0x9c, 0xce, 0xe6, 0xfe, 0xde, 0x8c, 0x0
};

const unsigned char PROGMEM knight_white_on_white[] = {
  8, 8,
  0x63, 0x9d, 0xce, 0xe6, 0xfe, 0xde, 0xad, 0x73
};

const unsigned char PROGMEM knight_black_on_white[] = {
  8, 8,
  0xff, 0x63, 0x31, 0x19, 0x1, 0x21, 0x73, 0xff
};

const unsigned char PROGMEM knight_black_on_black[] = {
  8, 8,
  0x9c, 0x62, 0x31, 0x19, 0x1, 0x21, 0x52, 0x8c
};

const unsigned char PROGMEM bishop_white_on_black[] = {
  8, 8,
  0x0, 0xb0, 0xfc, 0xfe, 0xce, 0xe4, 0xb0, 0x0
};

const unsigned char PROGMEM bishop_white_on_white[] = {
  8, 8,
  0x4f, 0xb3, 0xfd, 0xfe, 0xce, 0xe5, 0xb3, 0x4f
};

const unsigned char PROGMEM bishop_black_on_white[] = {
  8, 8,
  0xff, 0x4f, 0x3, 0x1, 0x31, 0x1b, 0x4f, 0xff
};

const unsigned char PROGMEM bishop_black_on_black[] = {
  8, 8,
  0xb0, 0x4c, 0x2, 0x1, 0x31, 0x1a, 0x4c, 0xb0
};

const unsigned char PROGMEM king_white_on_black[] = {
  8, 8,
  0x0, 0x98, 0xba, 0xff, 0xfa, 0xb8, 0x98, 0x0
};

const unsigned char PROGMEM king_white_on_white[] = {
  8, 8,
  0xe7, 0x99, 0xba, 0xff, 0xfa, 0xb9, 0x9b, 0xe3
};

const unsigned char PROGMEM king_black_on_white[] = {
  8, 8,
  0xff, 0x67, 0x45, 0x0, 0x5, 0x47, 0x67, 0xff
};

const unsigned char PROGMEM king_black_on_black[] = {
  8, 8,
  0x18, 0x66, 0x45, 0x0, 0x5, 0x46, 0x64, 0x1c
};

const unsigned char PROGMEM queen_white_on_black[] = {
  8, 8,
  0x4, 0x98, 0xbc, 0xf8, 0xf8, 0xbc, 0x98, 0x4
};

const unsigned char PROGMEM queen_white_on_white[] = {
  8, 8,
  0xe5, 0x9b, 0xbd, 0xfb, 0xfb, 0xbd, 0x9b, 0xe5
};

const unsigned char PROGMEM queen_black_on_white[] = {
  8, 8,
  0xfb, 0x67, 0x43, 0x7, 0x7, 0x43, 0x67, 0xfb
};

const unsigned char PROGMEM queen_black_on_black[] = {
  8, 8,
  0x1a, 0x64, 0x42, 0x4, 0x4, 0x42, 0x64, 0x1a
};

typedef enum piece_type { PAWN, CASTLE, KNIGHT, BISHOP, KING, QUEEN } PieceType;
typedef enum side { SIDE_WHITE, SIDE_BLACK } Side;

struct piece {
  PieceType piece_type;
  Side side;
  int row;
  int col;
};

typedef struct piece Piece;

Piece pieces[32];

int cursor_row = 0;
int cursor_col = 0;

bool board_get_color(int row, int col) {
  if (get_arity(row + col) == AR_EVEN) {
    return BOARD_WHITE;
  } else {
    return BOARD_BLACK;
  }
}

void tile_draw(int row, int col, const char* sprite) {

  // Since we are rotating the board visually 90deg clockwise,
  // "row" and "col" flip
  int _row = col;
  int _col = 8 - row;
  Sprites::drawOverwrite(_col * 8 + 32, _row * 8, sprite, 0);
}

void board_draw() {

  // "row" and "col" refer to the board row and columns if white were
  // on the bottom of the board.
  for (int row = 0; row < 8; row++) {
    for (int col = 0; col < 8; col++) {
      bool color = board_get_color(row, col);
      if (color == BOARD_WHITE) {
        tile_draw(row, col, WHITE_TILE);
      }
    }
  }
}

void piece_draw(struct piece* piece) {
  const bool board_color = board_get_color(piece->row, piece->col);
  switch (piece->piece_type) {
    case PAWN:
      if (piece->side == SIDE_BLACK) {
        if (board_color == BOARD_WHITE) {
          tile_draw(piece->row, piece->col, pawn_black_on_white);
        } else {
          tile_draw(piece->row, piece->col, pawn_black_on_black);
        }
      } else {
        if (board_color == BOARD_BLACK) {
          tile_draw(piece->row, piece->col, pawn_white_on_black);
        } else {
          tile_draw(piece->row, piece->col, pawn_white_on_white);
        }
      }
      break;
    case CASTLE:
      if (piece->side == SIDE_BLACK) {
        if (board_color == BOARD_WHITE) {
          tile_draw(piece->row, piece->col, castle_black_on_white);
        } else {
          tile_draw(piece->row, piece->col, castle_black_on_black);
        }
      } else {
        if (board_color == BOARD_BLACK) {
          tile_draw(piece->row, piece->col, castle_white_on_black);
        } else {
          tile_draw(piece->row, piece->col, castle_white_on_white);
        }
      }
      break;
    case KNIGHT:
      if (piece->side == SIDE_BLACK) {
        if (board_color == BOARD_WHITE) {
          tile_draw(piece->row, piece->col, knight_black_on_white);
        } else {
          tile_draw(piece->row, piece->col, knight_black_on_black);
        }
      } else {
        if (board_color == BOARD_BLACK) {
          tile_draw(piece->row, piece->col, knight_white_on_black);
        } else {
          tile_draw(piece->row, piece->col, knight_white_on_white);
        }
      }
      break;
    case BISHOP:
      if (piece->side == SIDE_WHITE) {
        if (board_color == BOARD_BLACK) {
          tile_draw(piece->row, piece->col, bishop_white_on_black);
        } else {
          tile_draw(piece->row, piece->col, bishop_white_on_white);
        }
      } else {
        if (board_color == BOARD_WHITE) {
          tile_draw(piece->row, piece->col, bishop_black_on_white);
        } else {
          tile_draw(piece->row, piece->col, bishop_black_on_black);
        }
      }
      break;
    case KING:
      if (piece->side == SIDE_WHITE) {
        if (board_color == BOARD_BLACK) {
          tile_draw(piece->row, piece->col, king_white_on_black);
        } else {
          tile_draw(piece->row, piece->col, king_white_on_white);
        }
      } else {
        if (board_color == BOARD_WHITE) {
          tile_draw(piece->row, piece->col, king_black_on_white);
        } else {
          tile_draw(piece->row, piece->col, king_black_on_black);
        }
      }
      break;
    case QUEEN:
      if (piece->side == SIDE_WHITE) {
        if (board_color == BOARD_BLACK) {
          tile_draw(piece->row, piece->col, queen_white_on_black);
        } else {
          tile_draw(piece->row, piece->col, queen_white_on_white);
        }
      } else {
        if (board_color == BOARD_WHITE) {
          tile_draw(piece->row, piece->col, queen_black_on_white);
        } else {
          tile_draw(piece->row, piece->col, queen_black_on_black);
        }
      }
  }
}

void cursor_draw() {
  const int _row = cursor_col;
  const int _col = 8 - cursor_row;
  const int cursor_x = _col * 8 + 32;
  const int cursor_y = _row * 8;

  if (board_get_color(cursor_row, cursor_col) == BOARD_WHITE) {
    arduboy.drawRect(cursor_x, cursor_y, 8, 8, 0);
  } else {
    arduboy.drawRect(cursor_x, cursor_y, 8, 8, 1);
  }
}

void init_pieces() {
  for (int i = 0; i < 8; i++) {
    pieces[i] = { PAWN, SIDE_BLACK, 1, i};
    pieces[i + 8] = { PAWN, SIDE_WHITE, 6, i};
  }

  pieces[16] = { CASTLE, SIDE_BLACK, 0, 0};
  pieces[17] = { CASTLE, SIDE_BLACK, 0, 7};
  pieces[18] = { KNIGHT, SIDE_BLACK, 0, 1};
  pieces[19] = { KNIGHT, SIDE_BLACK, 0, 6};
  pieces[20] = { BISHOP, SIDE_BLACK, 0, 2};
  pieces[21] = { BISHOP, SIDE_BLACK, 0, 5};
  pieces[22] = { QUEEN, SIDE_BLACK, 0, 3};
  pieces[23] = { KING, SIDE_BLACK, 0, 4};

  pieces[24] = { CASTLE, SIDE_WHITE, 7, 0};
  pieces[25] = { CASTLE, SIDE_WHITE, 7, 7};
  pieces[26] = { KNIGHT, SIDE_WHITE, 7, 1};
  pieces[27] = { KNIGHT, SIDE_WHITE, 7, 6};
  pieces[28] = { BISHOP, SIDE_WHITE, 7, 2};
  pieces[29] = { BISHOP, SIDE_WHITE, 7, 5};
  pieces[30] = { QUEEN, SIDE_WHITE, 7, 3};
  pieces[31] = { KING, SIDE_WHITE, 7, 4};
}

void setup() {
  Serial.print("setting up");
  Serial.println();

  arduboy.begin();
  arduboy.setFrameRate(30);

  init_pieces();
  Serial.print("Done setting up");
  Serial.println();

  board_draw();
}

void loop() {
  if (!(arduboy.nextFrame()))
    return;

  arduboy.pollButtons();
  if (arduboy.justPressed(DOWN_BUTTON) && cursor_col < 7) {
    cursor_col++;
  }
  if (arduboy.justPressed(UP_BUTTON) && cursor_col > 0) {
    cursor_col--;
  }
  if (arduboy.justPressed(LEFT_BUTTON) && cursor_row < 7) {
    cursor_row++;
  }
  if (arduboy.justPressed(RIGHT_BUTTON) && cursor_row > 0) {
    cursor_row--;
  }

  arduboy.clear();
  board_draw();

  for (int i = 0; i < 32; i++) {
    Piece piece = pieces[i];

    piece_draw(&piece);
  }

  cursor_draw();

  arduboy.display();
}
