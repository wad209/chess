set(ARDUINO_LIB_PATH "$ENV{HOME}/Arduino/libraries" CACHE PATH
    "Path of the Arduino user installed libraries folder, e.g. ~/Arduino/libraries.")

set(ARDUBOY2_LIB_PATH "${ARDUINO_LIB_PATH}/Arduboy2/src" CACHE PATH
    "Path of the Arduino user installed libraries folder, e.g. ~/Arduino/libraries.")

add_library(Arduboy2 STATIC
    ${ARDUBOY2_LIB_PATH}/Arduboy2.cpp
    ${ARDUBOY2_LIB_PATH}/Arduboy2.h
    ${ARDUBOY2_LIB_PATH}/Arduboy2Audio.cpp
    ${ARDUBOY2_LIB_PATH}/Arduboy2Audio.h
    ${ARDUBOY2_LIB_PATH}/Arduboy2Beep.cpp
    ${ARDUBOY2_LIB_PATH}/Arduboy2Beep.h
    ${ARDUBOY2_LIB_PATH}/Arduboy2Core.cpp
    ${ARDUBOY2_LIB_PATH}/Arduboy2Core.h
    ${ARDUBOY2_LIB_PATH}/Arduboy2Data.cpp
    ${ARDUBOY2_LIB_PATH}/Sprites.cpp
    ${ARDUBOY2_LIB_PATH}/Sprites.h
    ${ARDUBOY2_LIB_PATH}/SpritesB.cpp
    ${ARDUBOY2_LIB_PATH}/SpritesB.h
    ${ARDUBOY2_LIB_PATH}/SpritesCommon.h
)
target_link_libraries(Arduboy2 PUBLIC ArduinoCore)
target_compile_features(Arduboy2 PUBLIC cxx_std_11 c_std_11)
