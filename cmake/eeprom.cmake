set(EEPROM_LIB_PATH "${ARDUINO_PATH}/hardware/avr/1.8.6/libraries/EEPROM/src/" CACHE PATH
    "Path of the Arduino EEPROM libraries folder")

add_library(Eeprom STATIC
    ${EEPROM_LIB_PATH}/EEPROM.h
)
target_link_libraries(Eeprom PUBLIC ArduinoCore)
target_compile_features(Eeprom PUBLIC cxx_std_11 c_std_11)
