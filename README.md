To configure, run

```
cmake -S . -B build -DARDUINO_PORT=/dev/ttyUSB0 \
    -DCMAKE_TOOLCHAIN_FILE=cmake/toolchain/leonardo.toolchain.cmake
    -DCMAKE_BUILD_TYPE=MinSizeRel
```

To compile, do

```
cmake --build build -j -t chess
```

Finally to upload, do

```
cmake --build build -j -t upload-chess
```
